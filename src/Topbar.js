import './css/Template.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
function Topbar() {
  return (
    <div className="Topbar">
      <header>
        <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
          <h1 className="navbar-brand col-md-3 col-lg-2 mr-0 px-3 mb-0" >
            <FontAwesomeIcon icon={["fab", "bootstrap"]} /> BS Grid Builder</h1>
        </nav>
      </header>
    </div>
  );
}

export default Topbar;
