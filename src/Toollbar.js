import './css/Toolbar.scss';

function Toolbar() {
  return (
    <div className="Toolbar row" >
      <div class="input-group my-1" id="childQtd">
        <div class="input-group-prepend">
          <button class="btn btn-outline-secondary" type="button" id="button-addon1">-</button>
        </div>
        <input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1" value="0"></input>
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" id="button-addon2">+</button>
        </div>
      </div>
      <div class="input-group my-1" id="size">
        <div class="input-group-prepend">
          <button class="btn btn-outline-secondary" type="button" id="button-addon1">-</button>
        </div>
        <input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1" value="12"></input>
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" id="button-addon2">+</button>
        </div>
      </div>
    </div>
  );
}

export default Toolbar;
