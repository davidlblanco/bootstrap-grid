import React, { Component } from "react";
import './../css/Toolbar.scss';

class ToolBar extends Component {
    state = {
    };
    render() {
        return (
            <div className="Toolbar col-12" >
                <div className="input-group my-1" id="childQtd">
                    <div className="input-group-prepend">
                        <button
                            className="btn btn-outline-secondary"
                            type="button"
                            onClick={this.props.removeColumn}
                        >-</button>
                    </div>
                    <input
                        type="text"
                        disabled="disabled"
                        className="form-control"
                        placeholder={this.props.columns}
                        aria-label="Example text with button addon"
                        aria-describedby="button-addon1"
                        value={this.props.columns}
                        onChange={this.props.columnQtd}></input>
                    <div className="input-group-append">
                        <button
                            className="btn btn-outline-secondary"
                            type="button"
                            onClick={this.props.addColumn}
                        >+</button>
                    </div>
                </div>
                <div className="input-group my-1" id="size">
                    <div className="input-group-prepend">
                        <button
                            className="btn btn-outline-secondary"
                            type="button"
                            onClick={this.props.smallerCollumn}
                        >-</button>
                    </div>
                    <input
                        type="text"
                        disabled="disabled"
                        className="form-control"
                        placeholder={this.props.columnSize}
                        aria-label="Example text with button addon"
                        aria-describedby="button-addon1"
                        value={this.props.columnSize}
                        onChange={this.props.columnChangeSize}></input>
                    <div className="input-group-append">
                        <button
                            className="btn btn-outline-secondary"
                            type="button"
                            onClick={this.props.biggerColumn}
                        >+</button>
                    </div>
                </div>
            </div>

        );
    }

}

export default ToolBar;