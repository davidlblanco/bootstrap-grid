import React, { Component } from "react";
import ToolBar from "./toolBar";



class Container extends Component {
    state = {
        columns: 0,
        columnSize: 0,
        items: []

    };

    // como o input ta disabled pode ser que nao use isso mais
    columnQtd = (e) => {
        this.setState({ columns: e.target.value >= 12 ? 12 : e.target.value < 0 ? 0 : e.target.value });
        this.addItems()
    }
    addColumn = () => {
        this.setState({ columns: this.state.columns >= 12 ? 12 : this.state.columns + 1 });
        if (this.state.columns < 12) {
            this.addItems(this.state.columns + 1);
        }
        console.log('add columns: ', this.state.columns)
    }
    removeColumn = () => {
        this.setState({ columns: this.state.columns <= 0 ? 0 : this.state.columns - 1 });
        this.addItems(this.state.columns - 1);
        console.log('remove columns: ', this.state.columns)
    }
    addItems = (number) => {
        this.state.items = []
        if (number >= 1) {
            for (let i = 1; i <= number; i++) {
                this.state.items.push(
                    <div key={i} className='test col' >
                        <Container key={i} className='test col' />
                        {i}
                    </div>
                )
            }
        }
    }


    //
    //column size
    //


    // como o input ta disabled pode ser que nao use isso mais
    columnChangeSize = (e) => {
        this.setState({ columnSize: e.target.value >= 12 ? 12 : e.target.value < 0 ? 0 : e.target.value });
        console.log(e.target.parentNode.parentNode.parentNode.parentNode);
        //daqui pra baixo vi ter que colocar em outra fução
        // this.changeSize(e, e.target.value);
    }
    biggerColumn = (e) => {
        this.setState({ columnSize: this.state.columnSize >= 12 ? 12 : this.state.columnSize + 1 });
        if (this.state.columnSize < 12) {
            this.changeSize(e, this.state.columnSize + 1);
        }
        console.log('add columnsize: ', this.state.columnSize)
    }
    smallerCollumn = (e) => {
        this.setState({ columnSize: this.state.columnSize <= 0 ? 0 : this.state.columnSize - 1 });
        this.changeSize(e, this.state.columnSize - 1);
        console.log('remove columnSize: ', this.state.columnSize)
    }
    changeSize = (e, number) => {
        console.log(typeof (this.state.columnSize))
        if (typeof (number) == "number" && number > 0) {
            e.target.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.className = 'col-' + number;
        }
        else {
            e.target.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.className = 'col';

        }
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <ToolBar
                        addColumn={this.addColumn}
                        removeColumn={this.removeColumn}
                        columnQtd={this.columnQtd}
                        columns={this.state.columns}
                        columnChangeSize={this.columnChangeSize}
                        columnSize={this.state.columnSize}
                        biggerColumn={this.biggerColumn}
                        smallerCollumn={this.smallerCollumn}
                    />
                    {this.state.items}
                </div>
            </div>

        );
    }

}

export default Container;